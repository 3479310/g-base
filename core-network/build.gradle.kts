import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties

plugins {
  id("com.android.library")
  id("kotlin-android")
  id("kotlin-kapt")
}

android {
  compileSdk = Versions.compileSdk
  buildToolsVersion = Versions.buildTools

  defaultConfig {
    minSdk = Versions.minSdk
    targetSdk = Versions.targetSdk

    testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    consumerProguardFiles("consumer-rules.pro")

    val apiKey = gradleLocalProperties(rootDir)["api_key"].toString()
    resValue("string", "api_key", apiKey)
  }

  buildTypes {
    create("leakManage")
    getByName("release") {
      isMinifyEnabled = false
      proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
    }
  }

  compileOptions {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
  }

  kotlinOptions {
    jvmTarget = JavaVersion.VERSION_1_8.toString()
  }

  testOptions {
    unitTests {
      isIncludeAndroidResources = true
    }
  }
}

dependencies {
  implementation(Dependencies.kotlin)
  implementation(Dependencies.coreKtx)
  implementation(Dependencies.coroutines)

  implementation(Dependencies.retrofit)
  implementation(Dependencies.retrofitGsonConverter)
  implementation(Dependencies.okHttpLoggingInterceptor)

  implementation(Dependencies.hilt)
  kapt(Dependencies.hiltCompiler)

  testImplementationsPack()
}
