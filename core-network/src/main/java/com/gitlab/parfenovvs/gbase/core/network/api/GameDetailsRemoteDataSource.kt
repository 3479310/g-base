package com.gitlab.parfenovvs.gbase.core.network.api

import com.gitlab.parfenovvs.gbase.core.network.model.GameDetailsDto
import com.gitlab.parfenovvs.gbase.core.network.model.ImageDto
import javax.inject.Inject

interface GameDetailsRemoteDataSource {

  suspend fun gameDetails(id: Long): GameDetailsDto

  suspend fun gameScreenshots(id: Long, number: Int): List<ImageDto>
}