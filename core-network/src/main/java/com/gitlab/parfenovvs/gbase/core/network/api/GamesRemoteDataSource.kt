package com.gitlab.parfenovvs.gbase.core.network.api

import com.gitlab.parfenovvs.gbase.core.network.api.params.GamesApiParams
import com.gitlab.parfenovvs.gbase.core.network.model.GameDto
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface GamesRemoteDataSource {

  fun updateParams(params: GamesApiParams, alreadyLoadedCount: Int)

  suspend fun initialLoading(params: GamesApiParams): List<GameDto>

  suspend fun loadMore(): List<GameDto>
}