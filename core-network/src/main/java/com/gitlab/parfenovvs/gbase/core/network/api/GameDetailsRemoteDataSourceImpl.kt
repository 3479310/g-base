package com.gitlab.parfenovvs.gbase.core.network.api

import com.gitlab.parfenovvs.gbase.core.network.model.GameDetailsDto
import com.gitlab.parfenovvs.gbase.core.network.model.ImageDto
import javax.inject.Inject

class GameDetailsRemoteDataSourceImpl @Inject constructor(
  private val api: RawgApi
) : GameDetailsRemoteDataSource {

  override suspend fun gameDetails(id: Long): GameDetailsDto = api.gameDetails(id)

  override suspend fun gameScreenshots(id: Long, number: Int): List<ImageDto> =
    api.gameScreenshots(id, number).results
}