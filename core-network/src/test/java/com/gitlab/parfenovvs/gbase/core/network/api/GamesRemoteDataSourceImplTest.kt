package com.gitlab.parfenovvs.gbase.core.network.api

import com.gitlab.parfenovvs.gbase.core.network.api.params.GamesApiParams
import com.gitlab.parfenovvs.gbase.core.network.model.GameDto
import com.gitlab.parfenovvs.gbase.core.network.model.base.PagedResponse
import com.google.common.truth.Truth.assertThat
import io.mockk.coEvery
import io.mockk.mockkClass
import io.mockk.unmockkAll
import kotlinx.coroutines.test.runTest

import org.junit.After
import org.junit.Before
import org.junit.Test

class GamesRemoteDataSourceImplTest {

  private lateinit var mockApi: RawgApi
  private lateinit var gamesRemoteDataSource: GamesRemoteDataSourceImpl

  @Before
  fun setUp() {
    mockApi = mockkClass(RawgApi::class)
    gamesRemoteDataSource = GamesRemoteDataSourceImpl(mockApi)
  }

  @After
  fun tearDown() {
    unmockkAll()
  }

  @Test
  fun updateParams_AlreadyLoadedZero_Page1() {
    gamesRemoteDataSource.updateParams(GamesApiParams(), 0)
    assertThat(gamesRemoteDataSource.page).isEqualTo(1)
  }

  @Test
  fun updateParams_AlreadyLoadedOne_Page1() {
    gamesRemoteDataSource.updateParams(GamesApiParams(), 1)
    assertThat(gamesRemoteDataSource.page).isEqualTo(1)
  }

  @Test
  fun updateParams_AlreadyLoadedLessThanPageSize_Page1() {
    gamesRemoteDataSource.updateParams(
      GamesApiParams(),
      GamesRemoteDataSourceImpl.DEFAULT_PAGE_SIZE - 1
    )
    assertThat(gamesRemoteDataSource.page).isEqualTo(1)
  }

  @Test
  fun updateParams_AlreadyLoadedEqualToPageSize_Page1() {
    gamesRemoteDataSource.updateParams(
      GamesApiParams(),
      GamesRemoteDataSourceImpl.DEFAULT_PAGE_SIZE
    )
    assertThat(gamesRemoteDataSource.page).isEqualTo(1)
  }

  @Test
  fun updateParams_AlreadyLoadedMoreThanPageSize_Page2() {
    gamesRemoteDataSource.updateParams(
      GamesApiParams(),
      GamesRemoteDataSourceImpl.DEFAULT_PAGE_SIZE + 1
    )
    assertThat(gamesRemoteDataSource.page).isEqualTo(2)
  }

  @Test(expected = IllegalArgumentException::class)
  fun updateParams_AlreadyLoadedLessZero_ThrowsException() {
    gamesRemoteDataSource.updateParams(GamesApiParams(), -1)
  }

  @Test
  fun initialLoading_Success_ReturnsGameList() = runTest {
    val count = 3
    val expectedResponse = PagedResponse(
      count = count,
      nextPageUrl = "",
      previousPageUrl = "",
      results = List(count) { GameDto(it.toLong(), "", "", "", 0L, 0f) }
    )
    coEvery { mockApi.games(any()) } returns expectedResponse
    val actualResponse = gamesRemoteDataSource.initialLoading(GamesApiParams())
    assertThat(actualResponse).isEqualTo(expectedResponse.results)
  }

  @Test
  fun loadMore_Success_ReturnsGameList() = runTest {
    val count = 3
    val expectedResponse = PagedResponse(
      count = count,
      nextPageUrl = "",
      previousPageUrl = "",
      results = List(count) { GameDto(it.toLong(), "", "", "", 0L, 0f) }
    )
    coEvery { mockApi.games(any()) } returns expectedResponse
    gamesRemoteDataSource.updateParams(GamesApiParams(), 0)
    val actualResponse = gamesRemoteDataSource.loadMore()
    assertThat(actualResponse).isEqualTo(expectedResponse.results)
  }

  @Test
  fun loadMore_Success_IncreasesPage() = runTest {
    coEvery { mockApi.games(any()) } returns mockkClass(PagedResponse::class, relaxed = true)
    gamesRemoteDataSource.updateParams(GamesApiParams(), 0)
    val currentPage = gamesRemoteDataSource.page
    gamesRemoteDataSource.loadMore()
    val newPage = gamesRemoteDataSource.page
    assertThat(newPage).isEqualTo(currentPage + 1)
  }

  @Test(expected = IllegalStateException::class)
  fun loadMore_NoParams_ThrowsError() = runTest {
    gamesRemoteDataSource.loadMore()
  }
}