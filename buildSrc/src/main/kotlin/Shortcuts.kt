import org.gradle.api.artifacts.dsl.DependencyHandler

fun DependencyHandler.testImplementationsPack() {
  add("testImplementation", TestDependencies.junit)
  add("testImplementation", TestDependencies.truth)
  add("testImplementation", TestDependencies.coroutinesTest)
  add("testImplementation", TestDependencies.mockk)
  add("testImplementation", TestDependencies.mockkJvm)
  add("testImplementation", TestDependencies.coreTesting)
}

fun DependencyHandler.androidImplementationsPack() {
  add("androidTestImplementation", TestDependencies.junit)
  add("androidTestImplementation", TestDependencies.truth)
  add("androidTestImplementation", TestDependencies.coroutinesTest)
  add("androidTestImplementation", TestDependencies.mockk)
  add("androidTestImplementation", TestDependencies.mockkJvm)
  add("androidTestImplementation", TestDependencies.coreTesting)
  add("androidTestImplementation", TestDependencies.androidExtJUnit)
  add("androidTestImplementation", TestDependencies.androidTestRunner)
  add("androidTestImplementation", TestDependencies.androidTestRules)
}