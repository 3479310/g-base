val versionMajor = 1
val versionMinor = 1
val versionPatch = 0
val versionBuild = System.getenv()["BUILD_NUMBER"]?.toInt() ?: 0

plugins {
  id("com.android.application")
  id("kotlin-android")
  id("kotlin-kapt")
  id("androidx.navigation.safeargs.kotlin")
  id("dagger.hilt.android.plugin")
}

android {
  compileSdk = Versions.compileSdk
  buildToolsVersion = Versions.buildTools

  defaultConfig {
    applicationId = "com.gitlab.parfenovvs.gbase"
    minSdk = Versions.minSdk
    targetSdk = Versions.targetSdk
    versionCode = versionMajor * 10000 + versionMinor * 1000 + versionPatch * 100 + versionBuild
    versionName = "${versionMajor}.${versionMinor}.${versionPatch}.${versionBuild}"
    project.ext.set("archivesBaseName", "g-base-" + defaultConfig.versionName)

    testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
  }

  buildTypes {
    getByName("debug") {
      isDebuggable = true
      applicationIdSuffix = ".debug"
      versionNameSuffix = "-DEBUG"
    }
    create("leakManage") {
      isDebuggable = true
      applicationIdSuffix = ".debug"
      versionNameSuffix = "-DEBUG"
      signingConfig = signingConfigs.getByName("debug")
      dependencies {
        implementation(Dependencies.leakCanary)
      }
    }
    getByName("release") {
      isMinifyEnabled = false
      proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
    }
  }

  buildFeatures {
    viewBinding = true
  }

  compileOptions {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
  }
  kotlinOptions {
    jvmTarget = JavaVersion.VERSION_1_8.toString()
  }
  kapt {
    correctErrorTypes = true
  }
}

dependencies {
  implementation(project(":core-network"))

  implementation(Dependencies.kotlin)
  implementation(Dependencies.coroutines)
  implementation(Dependencies.timber)

  implementation(Dependencies.appcompat)
  implementation(Dependencies.coreKtx)
  implementation(Dependencies.constraintLayout)
  implementation(Dependencies.material)
  implementation(Dependencies.fragment)

  implementation(Dependencies.navFragmentKtx)
  implementation(Dependencies.navUiKtx)

  implementation(Dependencies.lifecycleViewModel)
  implementation(Dependencies.lifecycleRuntime)
  kapt(Dependencies.lifecycleCompiler)

  implementation(Dependencies.adapterDelegates)
  implementation(Dependencies.adapterDelegatesViewBinding)

  implementation(Dependencies.glide)
  kapt(Dependencies.glideCompiler)

  implementation(Dependencies.hilt)
  kapt(Dependencies.hiltCompiler)

  implementation(Dependencies.room)
  kapt(Dependencies.roomCompiler)

  testImplementationsPack()
  androidImplementationsPack()
}
