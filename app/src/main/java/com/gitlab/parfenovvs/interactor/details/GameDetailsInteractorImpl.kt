package com.gitlab.parfenovvs.interactor.details

import com.gitlab.parfenovvs.repository.details.GameDetailsRepository
import com.gitlab.parfenovvs.viewmodel.details.GameDetailsScreenModel
import javax.inject.Inject

class GameDetailsInteractorImpl @Inject constructor(
  private val initialModel: GameDetailsScreenModel.Initial,
  private val repository: GameDetailsRepository,
) : GameDetailsInteractor {

  override suspend fun data(): GameDetailsScreenModel =
    try {
      val model = repository.data(initialModel.id)
      GameDetailsScreenModel.from(model)
    } catch (e: Exception) {
      GameDetailsScreenModel.from(initialModel, e)
    }
}