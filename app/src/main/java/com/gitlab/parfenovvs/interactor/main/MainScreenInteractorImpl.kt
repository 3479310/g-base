package com.gitlab.parfenovvs.interactor.main

import com.gitlab.parfenovvs.model.CategoryType
import com.gitlab.parfenovvs.repository.GameCategoryRepository
import com.gitlab.parfenovvs.ui.base.model.base.ListItem
import com.gitlab.parfenovvs.ui.base.model.game.ErrorThinItem
import com.gitlab.parfenovvs.ui.base.model.game.ErrorWideItem
import com.gitlab.parfenovvs.ui.base.model.game.MainScreenErrorItem
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject
import javax.inject.Named

class MainScreenInteractorImpl @Inject constructor(
  @Named("mostAnticipated") private val mostAnticipatedGamesRepository: GameCategoryRepository,
  @Named("latestReleases") private val latestReleasesGamesRepository: GameCategoryRepository,
  @Named("rated") private val ratedGamesRepository: GameCategoryRepository,
) : MainScreenInteractor {

  override fun data(): Flow<List<ListItem>> = combine(
    mostAnticipatedGamesRepository.data(),
    latestReleasesGamesRepository.data(),
    ratedGamesRepository.data()
  ) { anticipated, latest, rated ->
    val list = listOf(
      GameCategoryMappers.mapToCategory(anticipated, true),
      GameCategoryMappers.mapToCategory(latest),
      GameCategoryMappers.mapToCategory(rated, true)
    )
    if (list.all { it.games.size == 1 && (it.games.first() is ErrorThinItem || it.games.first() is ErrorWideItem) }) {
      listOf(MainScreenErrorItem)
    } else list
  }

  override suspend fun initCategory(category: CategoryType) {
    when (category) {
      is CategoryType.MostAnticipated -> mostAnticipatedGamesRepository.init()
      is CategoryType.LatestReleases -> latestReleasesGamesRepository.init()
      is CategoryType.Rated -> ratedGamesRepository.init()
    }
  }

  override suspend fun tryToLoadMore(category: CategoryType, index: Int) {
    when (category) {
      is CategoryType.MostAnticipated -> mostAnticipatedGamesRepository.tryToLoadMore(index)
      is CategoryType.LatestReleases -> latestReleasesGamesRepository.tryToLoadMore(index)
      is CategoryType.Rated -> ratedGamesRepository.tryToLoadMore(index)
    }
  }

  override suspend fun refresh(category: CategoryType?) {
    when (category) {
      is CategoryType.MostAnticipated -> mostAnticipatedGamesRepository.refresh(false)
      is CategoryType.LatestReleases -> latestReleasesGamesRepository.refresh(false)
      is CategoryType.Rated -> ratedGamesRepository.refresh(false)
      null -> {
        coroutineScope {
          awaitAll(
            async { mostAnticipatedGamesRepository.refresh(true) },
            async { latestReleasesGamesRepository.refresh(true) },
            async { ratedGamesRepository.refresh(true) },
          )
        }
      }
    }
  }
}