package com.gitlab.parfenovvs.persist

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gitlab.parfenovvs.persist.model.GameEntity

@Database(entities = [GameEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
  abstract fun gamesDao(): GamesDao
}