package com.gitlab.parfenovvs.persist

import com.gitlab.parfenovvs.gbase.core.network.model.GameDto
import com.gitlab.parfenovvs.persist.model.GameEntity
import com.gitlab.parfenovvs.model.CategoryType
import javax.inject.Inject

interface GamesPersistDataSource {

  fun games(categoryType: CategoryType): List<GameDto>

  fun insert(games: List<GameDto>, categoryType: CategoryType)

  fun clear(categoryType: CategoryType)
}