package com.gitlab.parfenovvs.ui.main

import com.bumptech.glide.RequestManager
import com.gitlab.parfenovvs.model.CategoryType
import com.gitlab.parfenovvs.ui.base.BaseDiffUtilItemCallback
import com.gitlab.parfenovvs.ui.base.model.base.ListItem
import com.gitlab.parfenovvs.ui.base.model.game.GameItem
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter

class GameCardsAdapter(
  glide: RequestManager,
  onReadyToLoadMore: (Int) -> Unit,
  onGameClick: (GameItem) -> Unit,
  onRefreshClick: (CategoryType) -> Unit,
) : AsyncListDifferDelegationAdapter<ListItem>(BaseDiffUtilItemCallback()) {

  init {
    delegatesManager.addDelegate(
      MainScreenDelegates.wideGameDelegate(
        glide,
        onReadyToLoadMore,
        onGameClick
      )
    )
      .addDelegate(MainScreenDelegates.thinGameDelegate(glide, onReadyToLoadMore, onGameClick))
      .addDelegate(MainScreenDelegates.wideProgressDelegate())
      .addDelegate(MainScreenDelegates.thinProgressDelegate())
      .addDelegate(MainScreenDelegates.wideErrorDelegate(onRefreshClick))
      .addDelegate(MainScreenDelegates.thinErrorDelegate(onRefreshClick))
  }

  override fun getItemId(position: Int): Long {
    return items[position].itemId
  }
}