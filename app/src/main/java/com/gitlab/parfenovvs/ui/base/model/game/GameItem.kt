package com.gitlab.parfenovvs.ui.base.model.game

import com.gitlab.parfenovvs.ui.base.model.base.ListItem

abstract class GameItem : ListItem {
  abstract val id: Long
  abstract val title: String
  abstract val image: String?
}