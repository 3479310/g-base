package com.gitlab.parfenovvs.ui.details

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.gitlab.parfenovvs.R
import com.gitlab.parfenovvs.databinding.FragmentGameDetailsBinding
import com.gitlab.parfenovvs.ui.base.BaseDialogFragment
import com.gitlab.parfenovvs.ui.base.viewBinding
import com.gitlab.parfenovvs.util.getScreenSize
import com.gitlab.parfenovvs.util.launchAndRepeatOnStart
import com.gitlab.parfenovvs.util.onClick
import com.gitlab.parfenovvs.viewmodel.details.GameDetailsScreenModel
import com.gitlab.parfenovvs.viewmodel.details.GameDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class GameDetailsFragment : BaseDialogFragment() {
  private val binding by viewBinding { FragmentGameDetailsBinding.bind(it) }
  private val viewModel by viewModels<GameDetailsViewModel>()
  private val adapter by lazy { ScreenshotsAdapter(glide) }
  private val glide by lazy { Glide.with(this) }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    viewModel.errorData.connectErrorData()
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_game_details, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    with(binding) {
      screenshotsRecyclerView.adapter = adapter
      refreshImageView.onClick { viewModel.refresh() }
      tryAgainTextView.onClick { viewModel.refresh() }

      viewModel.data.collectWhileStarted {
        titleTextView.text = it.title
        refreshImageView.isVisible = false
        tryAgainTextView.isVisible = false

        val radius = resources.getDimensionPixelOffset(R.dimen.game_card_radius).toFloat()
        val screenSize = getScreenSize(requireContext())
        glide.load(it.image)
          .override(screenSize.x, (screenSize.y * 9f / 16).toInt())
          .transform(CenterCrop(), GranularRoundedCorners(0f, 0f, radius, radius))
          .transition(DrawableTransitionOptions.withCrossFade())
          .placeholder(R.drawable.bg_cover_placeholder)
          .into(coverImageView)

        if (it is GameDetailsScreenModel.Content) {
          descriptionTextView.text = Html.fromHtml(it.description) //ignore deprecation here
          adapter.items = it.screenshots
        } else if (it is GameDetailsScreenModel.Error) {
          refreshImageView.isVisible = true
          tryAgainTextView.isVisible = true
        }
      }
    }
  }
}