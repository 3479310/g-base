package com.gitlab.parfenovvs.ui.details

import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.gitlab.parfenovvs.R
import com.gitlab.parfenovvs.databinding.ItemScreenshotBinding
import com.gitlab.parfenovvs.ui.base.model.base.ListItem
import com.gitlab.parfenovvs.ui.base.model.game.ScreenshotItem
import com.gitlab.parfenovvs.ui.base.model.game.ScreenshotPlaceholderItem
import com.gitlab.parfenovvs.util.release
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding

class ScreenshotsAdapter(glide: RequestManager) : ListDelegationAdapter<List<ListItem>>() {

  init {
    delegatesManager
      .addDelegate(screenshotDelegate(glide))
      .addDelegate(screenshotPlaceholderDelegate())
  }

  private fun screenshotDelegate(glide: RequestManager) =
    adapterDelegateViewBinding<ScreenshotItem, ListItem, ItemScreenshotBinding>(
      { inflater, container -> ItemScreenshotBinding.inflate(inflater, container, false) }
    ) {
      bind {
        val resources = binding.root.resources
        glide.load(item.image)
          .override(
            resources.getDimensionPixelOffset(R.dimen.game_card_wide_width),
            resources.getDimensionPixelOffset(R.dimen.game_card_wide_height)
          )
          .transform(
            CenterCrop(),
            RoundedCorners(resources.getDimensionPixelOffset(R.dimen.game_card_radius))
          )
          .transition(DrawableTransitionOptions.withCrossFade())
          .placeholder(R.drawable.bg_item_placeholder)
          .into(binding.imageView)
      }

      onViewRecycled { glide.release(binding.imageView) }
    }

  private fun screenshotPlaceholderDelegate() =
    adapterDelegateViewBinding<ScreenshotPlaceholderItem, ListItem, ItemScreenshotBinding>(
      { inflater, container -> ItemScreenshotBinding.inflate(inflater, container, false) }
    ) {
      bind {
        binding.imageView.setBackgroundResource(R.drawable.bg_item_placeholder)
      }
    }
}