package com.gitlab.parfenovvs.ui.base.model.game

import com.gitlab.parfenovvs.model.CategoryType
import com.gitlab.parfenovvs.ui.base.model.base.ListItem

data class ErrorThinItem(val categoryType: CategoryType) : ListItem {
  override val itemId: Long = 1
}