package com.gitlab.parfenovvs.ui.base.model.base

interface ListItem {
  val itemId: Long
}