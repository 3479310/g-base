package com.gitlab.parfenovvs.ui.base.model.game

import com.gitlab.parfenovvs.ui.base.model.base.ListItem

data class ScreenshotItem(val image: String): ListItem {
  override val itemId: Long = image.hashCode().toLong()
}