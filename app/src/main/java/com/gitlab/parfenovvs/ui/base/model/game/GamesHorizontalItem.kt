package com.gitlab.parfenovvs.ui.base.model.game

import com.gitlab.parfenovvs.ui.base.model.base.ListItem
import com.gitlab.parfenovvs.model.CategoryType

data class GamesHorizontalItem(
  val title: String,
  val category: CategoryType,
  val games: List<ListItem>
) : ListItem {
  override val itemId: Long = title.hashCode().toLong()
}