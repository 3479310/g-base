package com.gitlab.parfenovvs.ui.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gitlab.parfenovvs.R
import com.gitlab.parfenovvs.databinding.FragmentMainBinding
import com.gitlab.parfenovvs.ui.base.model.game.GameItem
import com.gitlab.parfenovvs.ui.base.BaseFragment
import com.gitlab.parfenovvs.ui.base.viewBinding
import com.gitlab.parfenovvs.viewmodel.main.MainScreenViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : BaseFragment(R.layout.fragment_main) {
  private val binding by viewBinding { FragmentMainBinding.bind(it) }
  private val viewModel by viewModels<MainScreenViewModel>()

  private val adapter by lazy {
    MainScreenAdapter(
      glide = Glide.with(this),
      scrollStates = viewModel.scrollStates,
      onItemBind = viewModel::initCategory,
      onReadyToLoadMore = viewModel::readyToLoadMore,
      onGameClick = ::onGameClick,
      onRefreshClick = viewModel::refresh,
    )
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    viewModel.errorData.connectErrorData()
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    with(binding) {
      recyclerView.adapter = adapter
      recyclerView.itemAnimator = null

      viewModel.data.collectWhileStarted { adapter.items = it }
    }
  }

  override fun onStop() {
    (binding.recyclerView.layoutManager as? LinearLayoutManager)?.let { lm ->
      val visibleRange = lm.findFirstVisibleItemPosition() .. lm.findLastVisibleItemPosition()
      for (i in visibleRange) {
        val state = binding.recyclerView.findViewHolderForAdapterPosition(i)
          ?.itemView
          ?.findViewById<RecyclerView>(R.id.recyclerView)
          ?.layoutManager
          ?.onSaveInstanceState()
        state?.let { viewModel.scrollStates[i] = it }
      }
    }
    super.onStop()
  }

  private fun onGameClick(game: GameItem) {
    findNavController().navigate(
      MainFragmentDirections.actionMainFragmentToGameDetailsFragment(
        gameId = game.id,
        gameTitle = game.title,
        gameImage = game.image,
      )
    )
  }
}