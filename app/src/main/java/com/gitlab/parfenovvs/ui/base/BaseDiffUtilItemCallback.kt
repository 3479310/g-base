package com.gitlab.parfenovvs.ui.base

import androidx.recyclerview.widget.DiffUtil
import com.gitlab.parfenovvs.ui.base.model.base.ListItem

open class BaseDiffUtilItemCallback : DiffUtil.ItemCallback<ListItem>() {
  override fun areItemsTheSame(oldItem: ListItem, newItem: ListItem): Boolean =
    oldItem.itemId == newItem.itemId

  override fun areContentsTheSame(oldItem: ListItem, newItem: ListItem): Boolean {
    return oldItem.equals(newItem)
  }
}