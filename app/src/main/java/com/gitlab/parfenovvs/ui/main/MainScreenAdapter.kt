package com.gitlab.parfenovvs.ui.main

import android.os.Parcelable
import com.bumptech.glide.RequestManager
import com.gitlab.parfenovvs.model.CategoryType
import com.gitlab.parfenovvs.ui.base.BaseDiffUtilItemCallback
import com.gitlab.parfenovvs.ui.base.model.base.ListItem
import com.gitlab.parfenovvs.ui.base.model.game.GameItem
import com.gitlab.parfenovvs.ui.base.model.game.GamesHorizontalItem
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter

class MainScreenAdapter(
  glide: RequestManager,
  scrollStates: MutableMap<Int, Parcelable>,
  onItemBind: (GamesHorizontalItem) -> Unit,
  onReadyToLoadMore: (CategoryType, Int) -> Unit,
  onGameClick: (GameItem) -> Unit,
  onRefreshClick: (CategoryType?) -> Unit,
) : AsyncListDifferDelegationAdapter<ListItem>(BaseDiffUtilItemCallback()) {
  init {
    delegatesManager.addDelegate(
      MainScreenDelegates.gamesHorizontalDelegate(
        glide = glide,
        scrollStates = scrollStates,
        onItemBind = onItemBind,
        onReadyToLoadMore = onReadyToLoadMore,
        onGameClick = onGameClick,
        onRefreshClick = onRefreshClick,
      )
    )
      .addDelegate(MainScreenDelegates.mainErrorDelegate(onRefreshClick))
  }
}