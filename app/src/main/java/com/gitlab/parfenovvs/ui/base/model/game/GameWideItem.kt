package com.gitlab.parfenovvs.ui.base.model.game

data class GameWideItem(
  override val id: Long,
  override val title: String,
  override val image: String?
) : GameItem() {
  override val itemId: Long = id
}