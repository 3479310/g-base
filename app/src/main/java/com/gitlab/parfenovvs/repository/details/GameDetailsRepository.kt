package com.gitlab.parfenovvs.repository.details

import com.gitlab.parfenovvs.model.GameDetailsModel

interface GameDetailsRepository {

  suspend fun data(id: Long): GameDetailsModel

}