package com.gitlab.parfenovvs.repository

import com.gitlab.parfenovvs.model.GameCategoryModel
import kotlinx.coroutines.flow.Flow

interface GameCategoryRepository {

  fun data(): Flow<GameCategoryModel>

  suspend fun init()

  suspend fun tryToLoadMore(index: Int)

  suspend fun refresh(force: Boolean)
}