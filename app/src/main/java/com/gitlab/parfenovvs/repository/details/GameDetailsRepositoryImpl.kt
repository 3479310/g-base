package com.gitlab.parfenovvs.repository.details

import com.gitlab.parfenovvs.gbase.core.network.api.GameDetailsRemoteDataSource
import com.gitlab.parfenovvs.model.GameDetailsModel
import javax.inject.Inject

class GameDetailsRepositoryImpl @Inject constructor(
  private val remoteDataSource: GameDetailsRemoteDataSource,
) : GameDetailsRepository {

  override suspend fun data(id: Long): GameDetailsModel {
    val details = remoteDataSource.gameDetails(id)
    val screenshots = remoteDataSource.gameScreenshots(id, DEFAULT_SCREENSHOTS_MAX_NUMBER)
    return GameDetailsModel(
      id = details.id,
      title = details.title,
      description = details.description,
      image = details.image,
      screenshots = screenshots.map { it.image },
    )
  }

  private companion object {
    const val DEFAULT_SCREENSHOTS_MAX_NUMBER = 5
  }
}