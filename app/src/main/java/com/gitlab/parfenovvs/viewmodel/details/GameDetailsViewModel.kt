package com.gitlab.parfenovvs.viewmodel.details

import androidx.lifecycle.viewModelScope
import com.gitlab.parfenovvs.interactor.details.GameDetailsInteractor
import com.gitlab.parfenovvs.viewmodel.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class GameDetailsViewModel @Inject constructor(
  private val initialModel: GameDetailsScreenModel.Initial,
  private val interactor: GameDetailsInteractor,
) : BaseViewModel() {

  private val _data = MutableStateFlow<GameDetailsScreenModel>(initialModel)
  val data: Flow<GameDetailsScreenModel> = _data

  init {
    init()
  }

  fun refresh() {
    _data.value = initialModel
    init()
  }

  private fun init() {
    viewModelScope.launch(Dispatchers.IO + exceptionHandler) {
      _data.value = interactor.data()
    }
  }
}