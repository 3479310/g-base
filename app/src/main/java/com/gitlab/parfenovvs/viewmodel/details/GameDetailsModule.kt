package com.gitlab.parfenovvs.viewmodel.details

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.gitlab.parfenovvs.gbase.core.network.api.RawgApi
import com.gitlab.parfenovvs.interactor.details.GameDetailsInteractor
import com.gitlab.parfenovvs.interactor.details.GameDetailsInteractorImpl
import com.gitlab.parfenovvs.repository.details.GameDetailsRepository
import com.gitlab.parfenovvs.repository.details.GameDetailsRepositoryImpl
import com.gitlab.parfenovvs.util.require
import dagger.*
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import dagger.multibindings.IntoMap
import timber.log.Timber

@Module
@InstallIn(ViewModelComponent::class)
abstract class GameDetailsModule {

  @Binds
  @ViewModelScoped
  abstract fun gameDetailsInteractor(interactor: GameDetailsInteractorImpl): GameDetailsInteractor

  @Binds
  @ViewModelScoped
  abstract fun gameDetailsRepository(repository: GameDetailsRepositoryImpl): GameDetailsRepository

  companion object {

    @Provides
    @ViewModelScoped
    fun provideInitialModel(handle: SavedStateHandle): GameDetailsScreenModel.Initial =
      GameDetailsScreenModel.from(handle)
  }

}