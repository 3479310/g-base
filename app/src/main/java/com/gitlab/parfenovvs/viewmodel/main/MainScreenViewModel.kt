package com.gitlab.parfenovvs.viewmodel.main

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.gitlab.parfenovvs.interactor.main.MainScreenInteractor
import com.gitlab.parfenovvs.ui.base.model.base.ListItem
import com.gitlab.parfenovvs.ui.base.model.game.*
import com.gitlab.parfenovvs.model.CategoryType
import com.gitlab.parfenovvs.ui.main.MainFragmentDirections
import com.gitlab.parfenovvs.viewmodel.base.BaseViewModel
import com.gitlab.parfenovvs.viewmodel.details.GameDetailsModule
import com.gitlab.parfenovvs.viewmodel.details.GameDetailsScreenModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MainScreenViewModel @Inject constructor(
  private val interactor: MainScreenInteractor,
) : BaseViewModel() {

  private val _data = MutableStateFlow<List<ListItem>>(emptyList())
  val data: Flow<List<ListItem>> = _data

  val scrollStates = mutableMapOf<Int, Parcelable>()

  init {
    viewModelScope.launch(Dispatchers.IO + exceptionHandler) {
      interactor.data().collect { _data.value = it }
    }
  }

  fun initCategory(item: GamesHorizontalItem) {
    viewModelScope.launch(Dispatchers.IO + exceptionHandler) {
      interactor.initCategory(item.category)
    }
  }

  fun readyToLoadMore(category: CategoryType, index: Int) {
    viewModelScope.launch(Dispatchers.IO + exceptionHandler) {
      interactor.tryToLoadMore(category, index)
    }
  }

  fun refresh(category: CategoryType?) {
    viewModelScope.launch(Dispatchers.IO + exceptionHandler) {
      interactor.refresh(category)
    }
  }
}