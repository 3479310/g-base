package com.gitlab.parfenovvs.viewmodel.main

import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.gitlab.parfenovvs.R
import com.gitlab.parfenovvs.gbase.core.network.api.GamesRemoteDataSource
import com.gitlab.parfenovvs.interactor.main.MainScreenInteractor
import com.gitlab.parfenovvs.interactor.main.MainScreenInteractorImpl
import com.gitlab.parfenovvs.model.CategoryType
import com.gitlab.parfenovvs.persist.GamesPersistDataSource
import com.gitlab.parfenovvs.repository.GameCategoryRepository
import com.gitlab.parfenovvs.repository.SpecificCategoryRepositoryImpl
import com.gitlab.parfenovvs.util.ResourceProvider
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Named

@Module
@InstallIn(ViewModelComponent::class)
abstract class MainScreenModule {

  @Binds
  @ViewModelScoped
  abstract fun mainScreenInteractor(interactor: MainScreenInteractorImpl): MainScreenInteractor

  companion object {

    @Provides
    @ViewModelScoped
    @Named("mostAnticipated")
    fun mostAnticipatedRepository(
      remoteDataSource: GamesRemoteDataSource,
      persistDataSource: GamesPersistDataSource,
      resources: ResourceProvider,
    ): GameCategoryRepository = SpecificCategoryRepositoryImpl(
      remoteDataSource,
      persistDataSource,
      resources.string(R.string.most_anticipated),
      CategoryType.MostAnticipated
    )

    @Provides
    @ViewModelScoped
    @Named("latestReleases")
    fun latestReleasesRepository(
      remoteDataSource: GamesRemoteDataSource,
      persistDataSource: GamesPersistDataSource,
      resources: ResourceProvider,
    ): GameCategoryRepository = SpecificCategoryRepositoryImpl(
      remoteDataSource,
      persistDataSource,
      resources.string(R.string.latest_releases),
      CategoryType.LatestReleases
    )

    @Provides
    @ViewModelScoped
    @Named("rated")
    fun ratedRepository(
      remoteDataSource: GamesRemoteDataSource,
      persistDataSource: GamesPersistDataSource,
      resources: ResourceProvider,
    ): GameCategoryRepository = SpecificCategoryRepositoryImpl(
      remoteDataSource,
      persistDataSource,
      resources.string(R.string.most_rated_in_2021),
      CategoryType.Rated
    )
  }

}