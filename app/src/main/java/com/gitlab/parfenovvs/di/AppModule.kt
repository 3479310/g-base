package com.gitlab.parfenovvs.di

import android.content.Context
import androidx.room.Room
import com.gitlab.parfenovvs.persist.AppDatabase
import com.gitlab.parfenovvs.persist.GamesDao
import com.gitlab.parfenovvs.persist.GamesPersistDataSource
import com.gitlab.parfenovvs.persist.GamesPersistDataSourceImpl
import com.gitlab.parfenovvs.util.AndroidResourceProvider
import com.gitlab.parfenovvs.util.ResourceProvider
import dagger.*
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

  @Binds
  abstract fun gamesPersistDataSource(impl: GamesPersistDataSourceImpl): GamesPersistDataSource

  companion object {

    @Provides
    @Singleton
    fun resourceProvider(@ApplicationContext context: Context): ResourceProvider =
      AndroidResourceProvider(context)

    @Provides
    fun gamesDao(@ApplicationContext context: Context): GamesDao =
      Room.databaseBuilder(context, AppDatabase::class.java, "database")
        .build()
        .gamesDao()
  }
}