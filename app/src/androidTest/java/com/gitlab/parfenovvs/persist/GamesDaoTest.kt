package com.gitlab.parfenovvs.persist

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.gitlab.parfenovvs.model.CategoryType
import com.gitlab.parfenovvs.persist.model.GameEntity
import com.google.common.truth.Truth.assertThat
import io.mockk.mockkClass
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class GamesDaoTest {

  private lateinit var db: AppDatabase
  private lateinit var dao: GamesDao

  @Before
  fun setUp() {
    val context = ApplicationProvider.getApplicationContext<Context>()
    db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
      .allowMainThreadQueries()
      .build()
    dao = db.gamesDao()
  }

  @After
  fun tearDown() {
    db.close()
  }

  @Test
  fun getMostAnticipated_Initial_ReturnsEmpty() {
    assertThat(dao.getMostAnticipated()).isEmpty()
  }

  @Test
  fun getMostAnticipated_ExistingData_ReturnsEntities() {
    val entities = List(3) {
      GameEntity(
        id = it.toLong(),
        title = "title",
        image = "imageUrl",
        categoryId = CategoryType.MostAnticipated.internalId,
        released = "released",
        added = 0L,
        rating = 0f,
      )
    }
    dao.insertAll(entities)

    val result = dao.getMostAnticipated()

    assertThat(result).isEqualTo(entities)
  }

  @Test
  fun getLatestReleases_Initial_ReturnsEmpty() {
    assertThat(dao.getLatestReleases()).isEmpty()
  }

  @Test
  fun getLatestReleases_ExistingData_ReturnsEntities() {
    val entities = List(3) {
      GameEntity(
        id = it.toLong(),
        title = "title",
        image = "imageUrl",
        categoryId = CategoryType.LatestReleases.internalId,
        released = "released",
        added = 0L,
        rating = 0f,
      )
    }
    dao.insertAll(entities)

    val result = dao.getLatestReleases()

    assertThat(result).isEqualTo(entities)
  }

  @Test
  fun getRated_Initial_ReturnsEmpty() {
    assertThat(dao.getRated()).isEmpty()
  }

  @Test
  fun getRated_ExistingData_ReturnsEntities() {
    val entities = List(3) {
      GameEntity(
        id = it.toLong(),
        title = "title",
        image = "imageUrl",
        categoryId = CategoryType.Rated.internalId,
        released = "released",
        added = 0L,
        rating = 0f,
      )
    }
    dao.insertAll(entities)

    val result = dao.getRated()

    assertThat(result).isEqualTo(entities)
  }

  @Test
  fun insertAll_Empty_Nothing() {
    dao.insertAll(emptyList())
  }

  @Test
  fun clear_ExistingData_EmptyDb() {
    val entities = List(3) {
      GameEntity(
        id = it.toLong(),
        title = "title",
        image = "imageUrl",
        categoryId = CategoryType.MostAnticipated.internalId,
        released = "released",
        added = 0L,
        rating = 0f,
      )
    }
    dao.insertAll(entities)

    dao.clear(entities.first().categoryId)

    assertThat(dao.getMostAnticipated()).isEmpty()
  }
}