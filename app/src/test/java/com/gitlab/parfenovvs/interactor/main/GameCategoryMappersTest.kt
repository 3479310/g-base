package com.gitlab.parfenovvs.interactor.main

import com.gitlab.parfenovvs.gbase.core.network.api.PagingState
import com.gitlab.parfenovvs.gbase.core.network.model.GameDto
import com.gitlab.parfenovvs.model.GameCategoryModel
import com.gitlab.parfenovvs.ui.base.model.game.ErrorThinItem
import com.gitlab.parfenovvs.ui.base.model.game.GameThinItem
import com.gitlab.parfenovvs.ui.base.model.game.ProgressThinItem
import com.google.common.truth.Truth.assertThat
import io.mockk.every
import io.mockk.mockkClass
import org.junit.Before

import org.junit.Test

class GameCategoryMappersTest {

  private lateinit var categoryModel: GameCategoryModel

  @Before
  fun setUp() {
    categoryModel = mockkClass(GameCategoryModel::class, relaxed = true)
  }

  @Test
  fun mapToCategory_InitialState_ReturnsProgressItems() {
    every { categoryModel.dataState } returns PagingState.Initial

    val result = GameCategoryMappers.mapToCategory(categoryModel)

    assertThat(result.games).hasSize(GameCategoryMappers.INITIAL_PROGRESS_RANGE.last)
    for (g in result.games) {
      assertThat(g).isInstanceOf(ProgressThinItem::class.java)
    }
  }

  @Test
  fun mapToCategory_ContentState_ReturnsGameItems() {
    val gameDto = mockkClass(GameDto::class, relaxed = true)
    every { categoryModel.dataState } returns PagingState.Content(listOf(gameDto))

    val result = GameCategoryMappers.mapToCategory(categoryModel)

    assertThat(result.games).hasSize(1)
    assertThat(result.games.first()).isInstanceOf(GameThinItem::class.java)
  }

  @Test
  fun mapToCategory_PagingState_ReturnsGameItemsWithProgressAtTheEnd() {
    val gameDto = mockkClass(GameDto::class, relaxed = true)
    every { categoryModel.dataState } returns PagingState.Paging(listOf(gameDto))

    val result = GameCategoryMappers.mapToCategory(categoryModel)

    assertThat(result.games.last()).isInstanceOf(ProgressThinItem::class.java)
  }

  @Test
  fun mapToCategory_PersistState_ReturnsGameItemsWithErrorAtTheEnd() {
    val gameDto = mockkClass(GameDto::class, relaxed = true)
    every { categoryModel.dataState } returns PagingState.Persist(listOf(gameDto))

    val result = GameCategoryMappers.mapToCategory(categoryModel)

    assertThat(result.games.last()).isInstanceOf(ErrorThinItem::class.java)
  }

  @Test
  fun mapToCategory_ErrorState_ReturnsErrorItem() {
    every { categoryModel.dataState } returns PagingState.Error(Exception())

    val result = GameCategoryMappers.mapToCategory(categoryModel)

    assertThat(result.games).hasSize(1)
    assertThat(result.games.first()).isInstanceOf(ErrorThinItem::class.java)
  }
}