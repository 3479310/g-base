package com.gitlab.parfenovvs.interactor.main

import com.gitlab.parfenovvs.gbase.core.network.api.PagingState
import com.gitlab.parfenovvs.gbase.core.network.model.GameDto
import com.gitlab.parfenovvs.model.CategoryType
import com.gitlab.parfenovvs.model.GameCategoryModel
import com.gitlab.parfenovvs.repository.GameCategoryRepository
import com.gitlab.parfenovvs.ui.base.model.game.MainScreenErrorItem
import com.google.common.truth.Truth.assertThat
import io.mockk.*
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test

class MainScreenInteractorImplTest {

  private lateinit var mockMostAnticipatedGamesRepository: GameCategoryRepository
  private lateinit var mockLatestReleasesGamesRepository: GameCategoryRepository
  private lateinit var mockRatedGamesRepository: GameCategoryRepository
  private lateinit var interactor: MainScreenInteractor

  @Before
  fun setUp() {
    mockMostAnticipatedGamesRepository = mockkClass(GameCategoryRepository::class)
    mockLatestReleasesGamesRepository = mockkClass(GameCategoryRepository::class)
    mockRatedGamesRepository = mockkClass(GameCategoryRepository::class)
    interactor = MainScreenInteractorImpl(
      mockMostAnticipatedGamesRepository,
      mockLatestReleasesGamesRepository,
      mockRatedGamesRepository
    )
  }

  @After
  fun tearDown() {
    unmockkAll()
  }

  @Test
  fun data_Success_ReturnsGameCategoriesWithGames() = runTest {
    val game = GameDto(0L, "", "", "", 0L, 0f)
    every { mockMostAnticipatedGamesRepository.data() } returns flow {
      emit(
        GameCategoryModel(
          title = "title",
          category = CategoryType.MostAnticipated,
          dataState = PagingState.Content(listOf(game))
        )
      )
    }
    every { mockLatestReleasesGamesRepository.data() } returns flow {
      emit(
        GameCategoryModel(
          title = "title",
          category = CategoryType.LatestReleases,
          dataState = PagingState.Content(listOf(game))
        )
      )
    }
    every { mockRatedGamesRepository.data() } returns flow {
      emit(
        GameCategoryModel(
          title = "title",
          category = CategoryType.Rated,
          dataState = PagingState.Content(listOf(game))
        )
      )
    }

    val result = interactor.data().first()

    assertThat(result).hasSize(3)
  }

  @Test
  fun data_Fail_ReturnsMainError() = runTest {
    every { mockMostAnticipatedGamesRepository.data() } returns flow {
      emit(
        GameCategoryModel(
          title = "title",
          category = CategoryType.MostAnticipated,
          dataState = PagingState.Error(Exception())
        )
      )
    }
    every { mockLatestReleasesGamesRepository.data() } returns flow {
      emit(
        GameCategoryModel(
          title = "title",
          category = CategoryType.LatestReleases,
          dataState = PagingState.Error(Exception())
        )
      )
    }
    every { mockRatedGamesRepository.data() } returns flow {
      emit(
        GameCategoryModel(
          title = "title",
          category = CategoryType.Rated,
          dataState = PagingState.Error(Exception())
        )
      )
    }

    val result = interactor.data().first()

    assertThat(result).hasSize(1)
    assertThat(result[0]).isInstanceOf(MainScreenErrorItem::class.java)
  }

  @Test
  fun initCategory_MostAnticipated_SuccessfulExecution() = runTest {
    coEvery { mockMostAnticipatedGamesRepository.init() } just Runs
    interactor.initCategory(CategoryType.MostAnticipated)
  }

  @Test
  fun initCategory_LatestReleases_SuccessfulExecution() = runTest {
    coEvery { mockLatestReleasesGamesRepository.init() } just Runs
    interactor.initCategory(CategoryType.LatestReleases)
  }

  @Test
  fun initCategory_MostRated_SuccessfulExecution() = runTest {
    coEvery { mockRatedGamesRepository.init() } just Runs
    interactor.initCategory(CategoryType.Rated)
  }

  @Test
  fun tryToLoadMore_MostAnticipated_SuccessfulExecution() = runTest {
    coEvery { mockMostAnticipatedGamesRepository.tryToLoadMore(any()) } just Runs
    interactor.tryToLoadMore(CategoryType.MostAnticipated, 0)
  }

  @Test
  fun tryToLoadMore_LatestReleases_SuccessfulExecution() = runTest {
    coEvery { mockLatestReleasesGamesRepository.tryToLoadMore(any()) } just Runs
    interactor.tryToLoadMore(CategoryType.LatestReleases, 0)
  }

  @Test
  fun tryToLoadMore_MostRated_SuccessfulExecution() = runTest {
    coEvery { mockRatedGamesRepository.tryToLoadMore(any()) } just Runs
    interactor.tryToLoadMore(CategoryType.Rated, 0)
  }

  @Test
  fun refresh_MostAnticipated_SuccessfulExecution() = runTest {
    coEvery { mockMostAnticipatedGamesRepository.refresh(any()) } just Runs
    interactor.refresh(CategoryType.MostAnticipated)
  }

  @Test
  fun refresh_LatestReleases_SuccessfulExecution() = runTest {
    coEvery { mockLatestReleasesGamesRepository.refresh(any()) } just Runs
    interactor.refresh(CategoryType.LatestReleases)
  }

  @Test
  fun refresh_MostRated_SuccessfulExecution() = runTest {
    coEvery { mockRatedGamesRepository.refresh(any()) } just Runs
    interactor.refresh(CategoryType.Rated)
  }

  @Test
  fun refresh_NullCategory_SuccessfulExecution() = runTest {
    coEvery { mockMostAnticipatedGamesRepository.refresh(any()) } just Runs
    coEvery { mockLatestReleasesGamesRepository.refresh(any()) } just Runs
    coEvery { mockRatedGamesRepository.refresh(any()) } just Runs
    interactor.refresh(null)
  }
}