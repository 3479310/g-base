package com.gitlab.parfenovvs.persist

import com.gitlab.parfenovvs.gbase.core.network.model.GameDto
import com.gitlab.parfenovvs.model.CategoryType
import com.gitlab.parfenovvs.persist.model.GameEntity
import com.google.common.truth.Truth.assertThat
import io.mockk.*

import org.junit.After
import org.junit.Before
import org.junit.Test

class GamesPersistDataSourceImplTest {

  private lateinit var mockGamesDao: GamesDao
  private lateinit var gamesPersistDataSource: GamesPersistDataSource

  @Before
  fun setUp() {
    mockGamesDao = mockkClass(GamesDao::class)
    gamesPersistDataSource = GamesPersistDataSourceImpl(mockGamesDao)
  }

  @After
  fun tearDown() {
    unmockkAll()
  }

  @Test
  fun games_MostAnticipated_ReturnsGames() {
    val entities = listOf(mockkClass(GameEntity::class, relaxed = true))

    every { mockGamesDao.getMostAnticipated() } returns entities

    assertThat(gamesPersistDataSource.games(CategoryType.MostAnticipated))
      .isEqualTo(
        entities.map {
          GameDto(
            id = it.id,
            title = it.title,
            image = it.image,
            added = it.added,
            released = it.released,
            rating = it.rating,
          )
        }
      )
  }

  @Test
  fun games_LatestReleases_ReturnsGames() {
    val entities = listOf(mockkClass(GameEntity::class, relaxed = true))

    every { mockGamesDao.getLatestReleases() } returns entities

    assertThat(gamesPersistDataSource.games(CategoryType.LatestReleases))
      .isEqualTo(
        entities.map {
          GameDto(
            id = it.id,
            title = it.title,
            image = it.image,
            added = it.added,
            released = it.released,
            rating = it.rating,
          )
        }
      )
  }

  @Test
  fun games_Rated_ReturnsGames() {
    val entities = listOf(mockkClass(GameEntity::class, relaxed = true))

    every { mockGamesDao.getRated() } returns entities

    assertThat(gamesPersistDataSource.games(CategoryType.Rated))
      .isEqualTo(
        entities.map {
          GameDto(
            id = it.id,
            title = it.title,
            image = it.image,
            added = it.added,
            released = it.released,
            rating = it.rating,
          )
        }
      )
  }

  @Test
  fun insert_JustRuns() {
    every { mockGamesDao.insertAll(any()) } just Runs
    gamesPersistDataSource.insert(
      listOf(mockkClass(GameDto::class, relaxed = true)),
      mockkClass(CategoryType::class, relaxed = true)
    )
  }

  @Test
  fun clear_JustRuns() {
    every { mockGamesDao.clear(any()) } just Runs
    gamesPersistDataSource.clear(mockkClass(CategoryType::class, relaxed = true))
  }
}