package com.gitlab.parfenovvs.repository.details

import com.gitlab.parfenovvs.gbase.core.network.api.GameDetailsRemoteDataSource
import com.gitlab.parfenovvs.gbase.core.network.model.GameDetailsDto
import com.gitlab.parfenovvs.gbase.core.network.model.GameDto
import com.gitlab.parfenovvs.gbase.core.network.model.ImageDto
import com.gitlab.parfenovvs.model.GameDetailsModel
import com.google.common.truth.Truth.assertThat
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockkClass
import io.mockk.unmockkAll
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test

class GameDetailsRepositoryImplTest {

  private lateinit var mockDataSource: GameDetailsRemoteDataSource
  private lateinit var repository: GameDetailsRepository

  @Before
  fun setUp() {
    mockDataSource = mockkClass(GameDetailsRemoteDataSource::class)
    repository = GameDetailsRepositoryImpl(mockDataSource)
  }

  @After
  fun tearDown() {
    unmockkAll()
  }

  @Test
  fun data_LoadDetailsAndScreenshots_ReturnsModel() = runTest {
    val id = 1L
    val details = mockkClass(GameDetailsDto::class, relaxed = true)
    every { details.id } returns id
    val screenshots = listOf(mockkClass(ImageDto::class, relaxed = true))

    coEvery { mockDataSource.gameDetails(id) } returns details
    coEvery { mockDataSource.gameScreenshots(id, any()) } returns screenshots

    val result = repository.data(id)
    assertThat(result).isEqualTo(
      GameDetailsModel(
        id = id,
        title = details.title,
        image = details.image,
        description = details.description,
        screenshots = screenshots.map { it.image },
      )
    )
  }
}