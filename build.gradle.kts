// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
  repositories {
    google()
    jcenter()
  }
  dependencies {
    classpath(BuildPlugins.kotlin)
    classpath(BuildPlugins.gradle)
    classpath(BuildPlugins.navigationSafeArgs)
    classpath(BuildPlugins.hilt)
  }
}

allprojects {
  repositories {
    google()
    jcenter()
  }
}
